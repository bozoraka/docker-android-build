# https://confluence.atlassian.com/bitbucket/use-docker-images-as-build-environments-792298897.html
FROM atlassian/default-image:2

# Sets language to UTF8 : this works in pretty much all cases
ENV LANG en_US.UTF-8

# Never ask for confirmations
ENV DEBIAN_FRONTEND noninteractive

# Install Android SDK
RUN mkdir -p /root/.android && touch /root/.android/repositories.cfg
RUN rm -rf /usr/local/android-sdk/ \
    && mkdir -p /usr/local/android-sdk/.android/ \
    && touch /usr/local/android-sdk/.android/repositories.cfg
ENV ANDROID_HOME /usr/local/android-sdk
RUN wget https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
RUN unzip sdk-tools-linux-*.zip -d $ANDROID_HOME/
RUN rm sdk-tools-linux-*.zip
RUN yes | $ANDROID_HOME/tools/bin/sdkmanager --licenses
RUN yes | $ANDROID_HOME/tools/bin/sdkmanager "build-tools;28.0.3"
RUN $ANDROID_HOME/tools/bin/sdkmanager "platforms;android-28" "extras;android;m2repository"

# Install fastlane
RUN apt-get update && apt-get install -y ruby ruby-dev
RUN gem install fastlane -NV

# Etc
ENV ANDROID_SDK_HOME $ANDROID_HOME
ENV PATH ${INFER_HOME}/bin:${PATH}
ENV PATH $PATH:$ANDROID_SDK_HOME/tools
ENV PATH $PATH:$ANDROID_SDK_HOME/platform-tools
ENV GRADLE_OPTS "-Xms1G -Xmx2G -XX:MaxMetaspaceSize=512m -XX:+UseConcMarkSweepGC -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8 -server"
RUN mkdir -p /root/.gradle \
    && echo 'org.gradle.caching=true\n\
org.gradle.daemon.idletimeout=60000' > /root/.gradle/gradle.properties
