# Docker for Android App Build

This repository contains a `Dockerfile` to create a Docker image to build Android apps.

```bash
docker pull bozoraka/android-build
```

Currently, there is only one tag: `latest`.

## How to make changes

- Install [docker-ce](https://docs.docker.com/install/).
- `git clone git@bitbucket.org:bozoraka/docker-android-build.git`
- Make necessary changes
- Build the image: `docker build -t bozoraka/android-build:latest <path-to-Dockerfile>`.
- Login to [docker hub](https://docs.docker.com/get-started/part2/#log-in-with-your-docker-id).
- Push the image: `docker push bozoraka/android-build:latest`.
